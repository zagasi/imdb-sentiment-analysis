import re, os, nltk, math
from nltk.stem import PorterStemmer
from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer
from nltk.classify import NaiveBayesClassifier
from nltk.sentiment.vader import SentimentIntensityAnalyzer


# Filling stopwords list
stopwords = [words.strip() for words in open(r'dataset\stopwords.txt')]

# Filling negative dataset
neg_raw = [line.strip() for line in open(r'dataset\neg.txt')]

# Filling positive dataset
pos_raw = [line.strip() for line in open(r'dataset\pos.txt')]

# Porter Steammer
PS = PorterStemmer()

# Feature Extraction
CV = CountVectorizer()
TFIDF = TfidfVectorizer()

def preprocessing(text):
    # Cleaning text
    text = re.sub(r'[\\\/\-]', '', text.lower())
    text = re.sub(r'[^a-zA-Z\' ]', ' ', text.lower())

    # Tokenize
    text = text.split()

    # Stopwords removal
    text = [words for words in text if words not in stopwords and len(words) > 2]

    # Steamming
    # text = [PS.stem(words) for words in text]

    return ' '.join(text)

def get_word_features(wordlist):
    wordlist = nltk.FreqDist(wordlist)
    word_features = wordlist.keys()
    return word_features

def get_words_in_tweets(tweets):
    all_words = []
    for (words, sentiment) in tweets:
        all_words.extend(words)
    return all_words

def extract_features(document):
    document_words = set(document)
    features = {}
    for word in word_features:
        features['contains(%s)' % word] = (word in document_words)
    return features

if __name__ == "__main__":
    print('Sample raw positive dataset : ')
    for index, line in enumerate(pos_raw):
        print('[{0}] {1}'.format(index, line))

    print('\nSample raw negative dataset : ')
    for index, line in enumerate(neg_raw):
        print('[{0}] {1}'.format(index, line))

    print('\n----------')

    pos_pre = [preprocessing(words) for words in pos_raw]
    print('\nSample positive dataset after preprocessing :')
    for index, line in enumerate(pos_pre):
        print('[{0}] {1}'.format(index, line))

    neg_pre = [preprocessing(words) for words in neg_raw]
    print('\nSample negative dataset after preprocessing :')
    for index, line in enumerate(neg_pre):
        print('[{0}] {1}'.format(index, line))

    print('\nTF-IDF Weight pos + neg : ')
    print(TFIDF.fit_transform(pos_pre+neg_pre).toarray())


